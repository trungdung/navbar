# JFW - Admin Tool 



## Getting started

This asp.net core web application is a web tool to manage the JFW application. The project links to JFW git repository on debug mode.
This asp.net project supports:
- jquery v3.5.1
- alpine.js v3.9.2
- TailwindCss v3.2.0
- PostCSS
- SCSS
- Blazor components

## How to use watch mode

If you want to use the watch mode, you need to run the application with the following command:

```
cd project-path
dotnet-watch.bat
```

The project will be compiled and run in watch mode.

**Note**: The project is initially built based on JFW big project solution, if you want to run the application in debug mode by using released dll version (not depend on Jexpa.Core project), you need to modify the project file:

```
<ItemGroup Condition="'$(Configuration)' == 'Debug'">
    <Reference Include="Jexpa.Core">
        <HintPath>$(ReleasedDllPath)Jexpa.Core.dll</HintPath>
    </Reference>
    <Reference Include="Jexpa.DAO">
        <HintPath>$(ReleasedDllPath)Jexpa.DAO.dll</HintPath>
    </Reference>
    <Reference Include="Jexpa.Database">
        <HintPath>$(ReleasedDllPath)Jexpa.Database.dll</HintPath>
    </Reference>
    <Reference Include="Jexpa.Model">
        <HintPath>$(ReleasedDllPath)Jexpa.Model.dll</HintPath>
    </Reference>
    <Reference Include="Jexpa.Repository">
        <HintPath>$(ReleasedDllPath)Jexpa.Repository.dll</HintPath>
    </Reference>
    <Reference Include="Jexpa.Utility">
        <HintPath>$(ReleasedDllPath)Jexpa.Utility.dll</HintPath>
    </Reference>
</ItemGroup>
```

## How to use Blazor Components

- [ ] [Prerender and integrate ASP.NET Core Razor components](https://docs.microsoft.com/en-US/aspnet/core/blazor/components/prerendering-and-integration?WT.mc_id=DT-MVP-5003978&view=aspnetcore-5.0&pivots=server)
- [ ] [Component Tag Help](https://docs.microsoft.com/en-US/aspnet/core/mvc/views/tag-helpers/built-in/component-tag-helper?WT.mc_id=DT-MVP-5003978&view=aspnetcore-5.0)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

using Jexpa.Core.EntityClasses;
using Jexpa.Core.MemoryClasses;
using Microsoft.AspNetCore.Http;

namespace AdminTool.Extensions
{
    public static class SessionExtensions
    {
        private static readonly string _jfwSessionKey = "jfw-session-id";
        private static readonly string _failedCounterSessionKey = "failed-counter";
        public static string GetJfwSessionId(this ISession aSession)
        {
            string jfwSessionId = aSession.GetString(_jfwSessionKey);
            if (string.IsNullOrEmpty(jfwSessionId))
            {
                aSession.SetString(_jfwSessionKey, aSession.Id);
                return aSession.Id;
            }
            return jfwSessionId;
        }

        public static CUser GetCurrentUser(this ISession aSession)
        {
            string jfwSessionId = GetJfwSessionId(aSession);
            if (!string.IsNullOrEmpty(jfwSessionId))
            {
                return CSessionManager.GetUser(jfwSessionId);
            }
            return null;
        }

        public static void SetCurrentUser(this ISession aSession, CUser aUser)
        {
            string jfwSessionId = GetJfwSessionId(aSession);
            if (!string.IsNullOrEmpty(jfwSessionId))
            {
                CSessionManager.SetUser(jfwSessionId, aUser);
            }
        }

        public static void EndSession(this ISession aSession)
        {
            string jfwSessionId = GetJfwSessionId(aSession);
            if (!string.IsNullOrEmpty(jfwSessionId))
            {
                CSessionManager.EndSession(jfwSessionId);
            }
            aSession.Clear();
        }

        public static int? GetFailedCounter(this ISession aSession)
        {
            if (string.IsNullOrEmpty(aSession.GetInt32(_failedCounterSessionKey).ToString()))
            {
                aSession.SetInt32(_failedCounterSessionKey, 0);
                return 0;
            }

            return (int)aSession.GetInt32(_failedCounterSessionKey);
        }

        public static void FailedCounter(this ISession aSession)
        {
            int failedCounter = (int)aSession.GetFailedCounter();
            aSession.SetInt32(_failedCounterSessionKey, ++failedCounter);
        }

    }
}
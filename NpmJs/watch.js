const chokidar = require('chokidar');
const { exec } = require("child_process");

const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv

let watch_path = ".";
let watch_paths = [];
let exclude_path = argv.exclude_path;
let tailwindcss_path = argv.tailwindcss_path;
let watch_extra_extensions = argv.filter_ext;

if (argv.path) {
    watch_path = argv.path;
}

if (!argv.recursive) {
    watch_path += "/**"
}

watch_paths.push(watch_path + "/*.scss");
watch_paths.push(watch_path + "/*.sass");

if (watch_extra_extensions) {
    watch_extra_extensions.split(",").forEach(function (filter) {
        watch_paths.push(watch_path + "/*." + filter);
    });
}

// Initialize watcher.
const watcher = chokidar.watch(watch_paths, {
    ignored: (path, stats) => {
        if (exclude_path) {
            if(Array.isArray(exclude_path)) {
                for (let i = 0; i < exclude_path.length; i++) {
                    if (path.startsWith(exclude_path[i])) {
                        return true;
                    }
                }
            } else {
                if (path.startsWith(exclude_path)) {
                    return true;
                }
            }
        }
    },
    persistent: true
});

if (exclude_path) {
        watcher.unwatch(exclude_path);
  
}

// Something to use when events are received.
const log = console.log.bind(console);
const error = console.error.bind(console);

log(`Watching files in: ${watch_path}/*.[scss,sass,${watch_extra_extensions}]`);
watcher
    .on('change', path => {
        if (tailwindcss_path && IsTailwindTemplateFile(path)) {
            log(`Tailwind template file [${path}] has been changed.`);
            BuildFile(tailwindcss_path);
        } else if (path.endsWith(".scss") || path.endsWith(".sass")) {
            log(`File [${path}] has been changed.`);
            BuildFile(path);
        }
    })

function BuildFile(path) {
    exec(`node build-css.js --src=${path}`, (err, stdout, stderr) => {
        if (err) {
            error(err);
            return;
        }
        log(stdout);
        if (stderr) {
            error(stderr);
        }
    });
}

function IsTailwindTemplateFile(file_path) {
    let ext_list = watch_extra_extensions.split(",");
    for (let i = 0; i < ext_list.length; i++) {
        if (file_path.endsWith(ext_list[i])) {
            return true;
        }
    }
    return false;
}

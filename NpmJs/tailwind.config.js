module.exports = {
  mode: 'jit',
  content: [
    '../Components/**/*.{html,js,cshtml,cs,razor}',
    '../Pages/**/*.{html,js,cshtml,cs,razor}',
    '../Shared/**/*.{html,js,cshtml,cs,razor}',
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

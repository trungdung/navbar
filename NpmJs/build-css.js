const fs = require('fs');
const sass = require('sass');
const postcss = require('postcss')
const tailwindcss = require('tailwindcss'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano');

const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv

const compileCss = async (src, dest) => {
    const compiledCssObj = sass.compile(src, {
        sourceMap: true
    });
    const result = await postcss([
        tailwindcss,
        autoprefixer,
        cssnano({
            preset: 'default',
        })
    ]).process(compiledCssObj.css,
        {
            from: src,
            to: dest,
            map:
            {
                prev: compiledCssObj.sourceMap,
                inline: false,
            }
        });
    return result;
};

function updateReferencePath(sources, reference_path) {
    for (let i = 0; i < sources.length; i++) {
        sources[i] = reference_path;
    }
}

function addSourceMapComment(compiled_css_obj, map_link) {
    if (map_link) {
        compiled_css_obj.css += `\n/*# sourceMappingURL=${map_link} */`;
    } else {
        compiled_css_obj.css += `\n/*# sourceMappingURL=${compiled_css_obj.opts.to}.map */`;
    }
}

let src = argv.src;
let dest = argv.dest;
let reference_path;
let map_link;

if (!dest) {
    const pos = src.lastIndexOf(".");
    dest = src.substring(0, pos < 0 ? src.length : pos) + ".css";
}

if (argv.enable_source_map) {
    reference_path = argv.reference_path;
    map_link = argv.map_link;
}

if (src) {
    compileCss(src, dest).then(result => {
        if (argv.enable_source_map) {
            if (reference_path && result.map._sources._array) {
                updateReferencePath(result.map._sources._array, reference_path);
            }

            if (result.map) {
                addSourceMapComment(result, map_link);
                fs.writeFileSync(result.opts.to + '.map', result.map.toString())
            }
        }
        fs.writeFileSync(result.opts.to, result.css);
        console.log(`Build-CSS: Compiled ${src} to ${dest}`);
    });
}
using AdminTool.Extensions.GoogleCaptcha;
using Jexpa.Core.EntityClasses;
using Jexpa.Core.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace AdminTool.Pages.Account
{
    public class RegisterModel : SharedPageModel
    {
        private readonly GoogleCaptchaService _service;

        public class InputModel
        {
            [Required(ErrorMessage = "The username must not null")]
            public string Username { get; set; }
            [Required(ErrorMessage = "The email must not null")]
            [DataType(DataType.EmailAddress)]
            public string Email { get; set; }
            [Required(ErrorMessage = "The password must not null")]
            [DataType(DataType.Password)]
            public string Password { get; set; }
            [Required(ErrorMessage = "The confirm password must not null")]
            [DataType(DataType.Password)]
            [Compare("Password", ErrorMessage = "Password and Confirm Password must match")]
            public string ConfirmPassword { get; set; }
            public string CaptchaToken { get; set; }
        }
        [BindProperty]
        public InputModel inputModel { get; set; }
        public CUser NewUser { get; set; }

        public RegisterModel(GoogleCaptchaService service)
        {
            _service = service;
        }
        public void OnGet()
        {
            if (IsAuthenticated)
            {
                Response.Redirect("/");
            }
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {

            var captchaResult = await _service.VerifyToken(inputModel.CaptchaToken);

            if (!captchaResult)
            {
                return Page();
            }
            if (ModelState.IsValid)
            {
                RegistrationStatus status = CUser.RegisterUser(BrandUrl, inputModel.Username, inputModel.Password, inputModel.Email);

                if (status == RegistrationStatus.Success)
                {
                    return RedirectToPage(returnUrl);
                }
            }
            return Page();
        }
    }
}

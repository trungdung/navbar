using AdminTool.Extensions;
using AdminTool.Extensions.GoogleCaptcha;
using AdminTool.Pages;
using Jexpa.Core.EntityClasses;
using Jexpa.Core.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AdminTool.Pages.Account
{
    public class LoginModel : SharedPageModel
    {
        private readonly GoogleCaptchaService _service;

        public LoginModel(GoogleCaptchaService service)
        {
            _service = service;
        }
        [BindProperty]
        public int Counter { get; set; } = 0;
        [BindProperty]
        public string Username { get; set; } = string.Empty;
        [BindProperty]
        public string Password { get; set; }
        [BindProperty]
        public string CaptchaToken { get; set; }
        public string ErrorMessage { get; set; } = string.Empty;

        public void OnGet()
        {
            if (IsAuthenticated)
            {
                Response.Redirect("/");
            }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            LoginStatus loginStatus;
          
            if (IsAuthenticated)
            {
                return RedirectToPage("/Index");
            }

            if (HttpContext.Session.GetFailedCounter() > 3)
            {
                var captchaResult = await _service.VerifyToken(CaptchaToken);

                if (!captchaResult)
                {
                    return Page();
                }
            }

            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                ErrorMessage = "Username and password are required.";
                return Page();
            }

            loginStatus = CUser.LoginWithUsername(BrandUrl, Username, Password);

            if (loginStatus == LoginStatus.Success)
            {
                HttpContext.Session.SetCurrentUser(CUser.GetByUsername(BrandUrl, Username));
                return RedirectToPage("/Index");
            }
            HttpContext.Session.FailedCounter();
            ErrorMessage = "Invalid username or password.";
            return Page();
        }
    }
}

using AdminTool.Extensions;
using AdminTool.Extensions.GoogleCaptcha;
using Jexpa.Core.EntityClasses;
using Jexpa.Core.Enums;
using Jexpa.Core.MemoryClasses;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace AdminTool.Pages.Account
{
    public class ForgotPasswordModel : SharedPageModel
    {
        private readonly GoogleCaptchaService _service;
        [Required]
        [BindProperty]
        public string Email { get; set; }
        [BindProperty]
        public string CaptchaToken { get; set; }
        public string Token { get; set; } = string.Empty;
        [BindProperty]
        public string ErrorMessage { get; set; } = string.Empty;

        public ForgotPasswordModel(GoogleCaptchaService service)
        {
            _service = service;
        }
        public void OnGet()
        {
            if (IsAuthenticated)
            {
                Response.Redirect("/");
            }
        }

        private async Task<bool> GenerateResetToken()
        {

            string errorInfo = string.Empty;
            if (string.IsNullOrEmpty(Email))
            {
                errorInfo = "<p>Please enter your email address.</p>";
            }
            else if (!(new EmailAddressAttribute().IsValid(Email)))
            {
                errorInfo = "<p>Please enter a valid email address.</p>";
            }
            else
            {
                Token = CUser.GenerateResetPasswordToken(BrandUrl, Email);
                if (string.IsNullOrEmpty(Token))
                {
                    errorInfo += "<p>There was an error generating your reset token. Please try again later.</p>";
                }
            }

            if (!string.IsNullOrEmpty(errorInfo))
            {
                ErrorMessage = errorInfo;
                return false;
            }
            return true;
        }

        public async Task<IActionResult> OnPostSendForgotPasswordEmail()
        {
            var captchaResult = await _service.VerifyToken(CaptchaToken);

            if (!captchaResult)
            {
                return Page();
            }

            if (await GenerateResetToken())
            {
                string nickname = CUser.GetByEmail(BrandUrl, Email).GetNickName();

                var templateDictionary = new Dictionary<string, object>
                {
                    { "BrandUrl", HttpContext.Request.Host },
                    { "NickName", nickname },
                    { "ResetPasswordLink", GetResetPasswordUrl() }
                };
                var cBrand = CBrand.GetByUrl(BrandUrl);
                var cPattern = CPattern.GetByCode("FORGOT_PASSWORD");
                CMail cMail = new CMail();
                cMail.AddFrom(cBrand.GetEmailSupportTitle(), cBrand.GetEmailSupport());
                cMail.AddTo(nickname ?? "", Email);
                cMail.SetSubject(cPattern.GetSubject(CountryCode.US));
                cMail.SetBody(cPattern.GetContent(CountryCode.US), templateDictionary);
                cMail.Send();
            }
            return Page();
        }

        private string GetResetPasswordUrl()
        {
            return $"{HttpContext.Request.Host}/account/reset-password?email={Email}&token={Uri.EscapeDataString(Token)}";
        }
    }
}

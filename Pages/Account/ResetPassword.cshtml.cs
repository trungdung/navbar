using AdminTool.Extensions.GoogleCaptcha;
using Jexpa.Core.EntityClasses;
using Jexpa.Core.Enums;
using Jexpa.Core.MemoryClasses;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AdminTool.Pages.Account
{
    public class ResetPasswordModel : SharedPageModel
    {
        private readonly GoogleCaptchaService _service;
        [BindProperty]
        public string Token { get; set; } = string.Empty;
        [BindProperty]
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; } = string.Empty;
        [BindProperty]
        [Required(ErrorMessage = "New Password is required")]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; } = string.Empty;
        [BindProperty]
        [Required(ErrorMessage = "Confirm Password is required")]
        [Display(Name = "Confirm Password")]
        [Compare(nameof(NewPassword), ErrorMessage = "Passwords don't match")]
        public string ConfirmPassword { get; set; } = string.Empty;
        [BindProperty]
        public string CaptchaToken { get; set; }
        public TokenStatus TokenStatus;
        public string ErrorMessage = string.Empty;

        public ResetPasswordModel(GoogleCaptchaService service)
        {
            _service = service;
        }
        public void OnGet()
        {
            StringValues values;
            string url = HttpContext.Request.QueryString.ToString();
            if (QueryHelpers.ParseQuery(url).TryGetValue("email", out values))
            {
                Email = values.FirstOrDefault();
            }
            if (QueryHelpers.ParseQuery(url).TryGetValue("token", out values))
            {
                Token = values.FirstOrDefault();
            }
            if(!string.IsNullOrEmpty(Email)&& !string.IsNullOrEmpty(Token))
            {
                TokenStatus = CToken.ValidateResetPasswordToken(CUser.GetByEmail(BrandUrl, Email), Token);
            }           
        }

        public async Task<IActionResult> OnPostChangePassword()
        {
            var captchaResult = await _service.VerifyToken(CaptchaToken);

            if (!captchaResult)
            {
                return Page();
            }

            var result = CUser.ResetPassword(BrandUrl, Email, Token, NewPassword);

            if (result != ResetPasswordStatus.Success)
            {
                ErrorMessage = $"<p>Got an error in calling ResetPassword: {result}</p>";
            }
            return RedirectToPage("/Account/Login");
        }
    }
}

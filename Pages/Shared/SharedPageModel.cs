using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using AdminTool.Extensions;
using Jexpa.Core.EntityClasses;

namespace AdminTool.Pages
{
    public class SharedPageModel : PageModel
    {
        public CUser CurrentUser { get => HttpContext.Session.GetCurrentUser(); set => HttpContext.Session.SetCurrentUser(value); }
        public bool IsAuthenticated { get => CurrentUser != null; }
        public string BrandUrl
        {
            get => string.Empty;
        }

        public IActionResult OnPostLogout()
        {
            HttpContext.Session.EndSession();
            return RedirectToPage("/Login");
        }
    }
}

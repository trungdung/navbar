set SolutionDir=..\

cmd /C dotnet msbuild /p:SolutionDir=%SolutionDir%
cd NpmJs
start cmd /k node watch.js --path=".." --tailwindcss_path="../wwwroot/styles/tailwind.scss" --filter_ext="html,js,cshtml,cs,razor" --exclude_path="../bin/" --exclude_path="../obj/" --exclude_path="../NpmJs/" 
cd ..
cmd /C dotnet watch run --property:DotNetWatchMode=true --property:SolutionDir=%SolutionDir%
